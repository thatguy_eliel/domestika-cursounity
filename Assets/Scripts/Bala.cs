﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    public int damage = 1;
    public float speed = 2f;
    public Vector2 direction;
    public float TiempoVida = 3f;
    private Color _ColorInicial = Color.white;
    public Color Kaboom;
    private SpriteRenderer _render;
    private float _Contador;
    private Rigidbody2D _Rigidbody;
    private bool _Returning;

    void Awake()
    {
        _render = GetComponent<SpriteRenderer>();
        _Rigidbody = GetComponent<Rigidbody2D>();
    }
    // Start is called before the first frame update
    void Start()
    {
        _Contador = Time.time;
        Destroy(this.gameObject, TiempoVida);
    }

    // Update is called once per frame
    void Update()
    { 
        float _TimeSinceStarted = Time.time - _Contador;
        float _porcentaje = _TimeSinceStarted / TiempoVida;
        _render.color = Color.Lerp(_ColorInicial, Kaboom, _porcentaje);
    }

    private void FixedUpdate()
    {
        Vector2 movement = direction.normalized * speed;
        _Rigidbody.velocity = movement;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_Returning == false && collision.CompareTag("Player"))
        {
            collision.SendMessageUpwards("AddDamage", damage);
            Destroy(gameObject);
        }
        if (_Returning == true && collision.CompareTag("Enemy"))
        {
            collision.SendMessageUpwards("AddDamage");
            Destroy(gameObject);
        }
    }

    private void AddDamage()
    {
        _Returning = true;
        direction = direction * -1f;
    }
}
