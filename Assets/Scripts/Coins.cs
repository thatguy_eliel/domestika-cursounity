﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    public int Value = 1;
    private SpriteRenderer _Renderer;
    private Collider2D _Collider;

    private void Awake()
    {
        _Renderer = GetComponent<SpriteRenderer>();
        _Collider = GetComponent<Collider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.SendMessageUpwards("AddCoins", Value);
            //Desactivar collider
            Debug.Log("Consegui una moneda");
            _Collider.enabled = false;

            _Renderer.enabled = false;

            Destroy(gameObject);
        }
    }
}
