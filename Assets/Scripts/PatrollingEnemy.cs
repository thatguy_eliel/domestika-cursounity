﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrollingEnemy : MonoBehaviour
{
    public float speed = 1f;
    public float wallAware = 0.5f;
    public float groundAware = 0.5f;
    public LayerMask groundLayer;
    public Transform GroundCheck;
    RaycastHit2D Hit;
    public float playerAware = 3f;
    public float aimingTime = 0.5f;
    public float TiempoEspera = 2f;
    //Objects
    private AudioSource _Audio;
    private Rigidbody2D _Rigidbody;
    private Animator _Animator;
    private Weapon _Weapon;

    //Movimiento
    private Vector2 _movement;
    private bool _MirandoDerecha;
    private bool _EstaAtacando=false;

    void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Animator = GetComponent<Animator>();
        _Weapon = GetComponentInChildren<Weapon>();
        _Audio = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (transform.localScale.x < 0f)
        {
            _MirandoDerecha = false;
        }
        else
        {
            _MirandoDerecha = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Ground Check
        Hit = Physics2D.Raycast(GroundCheck.position, -transform.up, groundAware, groundLayer);
        //Wall Check
        Vector2 direccion = Vector2.right;
        if (_MirandoDerecha == false)
        {
            direccion = Vector2.left;
        }
        if (_EstaAtacando == false)
        {
            if (Physics2D.Raycast(transform.position, direccion, wallAware, groundLayer))
            {
                Flip();
            }
        }
    }

    private void FixedUpdate()
    {
        //Cerca de una pared
        float HorizontalVelocity = speed;
        if (_MirandoDerecha == false)
        {
            HorizontalVelocity = HorizontalVelocity * -1f;
        }

        //Si esta atacando
        if (_EstaAtacando == true)
        {
            HorizontalVelocity = 0f;
        }
        _Rigidbody.velocity = new Vector2(HorizontalVelocity, _Rigidbody.velocity.y);

        //No tocando el suelo
        if(Hit.collider == false)
        {
            Flip();
        }
    }

    private void LateUpdate()
    {
        _Animator.SetBool("Idle", _Rigidbody.velocity == Vector2.zero);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (_EstaAtacando == false && collision.CompareTag("Player"))
        {
            StartCoroutine("ApuntaYDispara");
        }
    }

    //Se encarga de darle la vuelta al enemigo teniendo la posicion que tenga
    private void Flip()
    {
        _MirandoDerecha = !_MirandoDerecha;
        float localScaleX = transform.localScale.x;
        localScaleX = localScaleX * -1f;
        transform.localScale = new Vector3(localScaleX, transform.localScale.y, transform.localScale.z);
    }

    private IEnumerator ApuntaYDispara()
    {
        _EstaAtacando = true;
        yield return new WaitForSeconds(aimingTime);
        _Animator.SetTrigger("Shoot");
        yield return new WaitForSeconds(TiempoEspera);

        _EstaAtacando = false;
    }
    void PuedeDisparar()
    {
        if (_Weapon != null)
        {
            _Weapon.Shoot();
            _Audio.Play();
        }
    }

    private void OnEnable()
    {
        _EstaAtacando = false;
    }

    private void OnDisable()
    {
        StopCoroutine("ApuntaYDispara");
        _EstaAtacando = false;
    }
}
