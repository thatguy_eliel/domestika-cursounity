﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject balaPrefab;
    public GameObject shooter;
    private Transform _FirePoint;

    void Awake()
    {
        _FirePoint = transform.Find("FirePoint");
    }

    // Start is called before the first frame update
    void Start()
    {
            //Invoke("Shoot", 1f);
            //Invoke("Shoot", 2f);
            //Invoke("Shoot", 3f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void Shoot()
    {
        if (balaPrefab != null && _FirePoint != null && shooter != null)
        {
            GameObject myBullet = Instantiate(balaPrefab, _FirePoint.position, Quaternion.identity) as GameObject;
            Bala bulletComponent = myBullet.GetComponent<Bala>();
            if(shooter.transform.localScale.x < 0f)
            {
                bulletComponent.direction = Vector2.left;
            }
            else
            {
                bulletComponent.direction = Vector2.right;
            }
        }
    }

}
