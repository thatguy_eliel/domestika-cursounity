﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private bool _IsAttacking;
    private Animator _Animator;
    private AudioSource _Audio;
    public AudioClip Attack;

    private void Awake()
    {
        _Animator = GetComponent<Animator>();
        _Audio = GetComponent<AudioSource>();
    }
    private void LateUpdate()
    {
        if (_Animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            _IsAttacking = true;
            _Audio.clip = Attack;
            _Audio.Play();
        }
        else
        {
            _IsAttacking = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(_IsAttacking == true)
        {
            if (collision.CompareTag("Enemy") || collision.CompareTag("BigBullet"))
            {
                collision.SendMessageUpwards("AddDamage");
            }
        }
    }
}
