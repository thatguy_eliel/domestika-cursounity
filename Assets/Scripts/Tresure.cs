﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tresure : MonoBehaviour
{
    private Rigidbody2D _Rigibody;
    public RectTransform LevelCompleated;
    public GameObject Enemies;
    public GameObject pc;

    private void Awake()
    {
        _Rigibody = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Uy, un tesoro!");
            LevelCompleated.gameObject.SetActive(true);
            Enemies.SetActive(false);
        }
    }
}
