﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{
    private Rigidbody2D _Rigidbody;
    public float FallTime = 1f;

    private void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Invoke("DropPlatform", FallTime);
            Destroy(gameObject, 2f);
        }
    }

    private void DropPlatform()
    {
        _Rigidbody.isKinematic = false;
    }
}
