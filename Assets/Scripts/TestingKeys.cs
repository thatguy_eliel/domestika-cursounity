﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingKeys : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //1.Boton del raton
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Se presiono el botón izquierdo del ratón");
        }
        if (Input.GetMouseButton(0))
        {
            Debug.Log("Se esta presionando el botón izquierdo del ratón");
        }
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("Se a presionado el botón izquierdo del ratón");
        }
            //1.1 El botón (1) sera el botón derecho del ratón, mientras que el botón (2) sera la rueda del ratón
        
        //2. Teclado
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Esto puede ser utilizado para hacer saltar al personaje--Keycode");//Este usa libreria especifica de C#
        }

        if (Input.GetButtonDown("Jump"))
        {
            Debug.Log("Esto puede ser utilizado para hacer saltar al personaje--Button");//Este usa libreria de Unity
        }

        //3. Obtener movimiento Axis
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (horizontal < 0f || horizontal > 0f)
        {
            Debug.Log("El eje horizontal es " + horizontal);
        }
        if (vertical <0f|| vertical > 0f)
        {
            Debug.Log("El eje vertical es " + vertical);
        }
    }
}
