﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int TotalHealth = 3;
    public RectTransform healthUI;
    public Transform SpawnPoint;
    //Game Over
    public RectTransform gameOverMenu;
    public GameObject Enemies;
    public GameObject Items;
    private Animator _Animator;
    private PlayerController _Controller;
    public AudioSource _Audio;
    public AudioClip MoreHealth;
    private Rigidbody2D _Rigidbody;

    private int _Health;
    private float heartSize = 16f;

    private SpriteRenderer _Renderer;

    private void Awake()
    {
        _Renderer = GetComponent<SpriteRenderer>();
        _Animator = GetComponent<Animator>();
        _Controller = GetComponent<PlayerController>();
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Audio = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        _Health = TotalHealth;
    }

   public void AddDamage(int amount)
    {
        _Health = _Health - amount;

        StartCoroutine("VisualFeedback");

        if(_Health <= 0)
        {
            _Health = 0;
            gameObject.SetActive(false);
        }
        healthUI.sizeDelta = new Vector2(heartSize * _Health, heartSize);
        Debug.Log("El jugador a sido lastimado, si salud actual es de " + _Health);
    }

    public void AddHealth(int amount)
    {
        _Health = _Health + amount;
        _Audio.clip = MoreHealth;
        _Audio.Play();
        if(_Health > TotalHealth)
        {
            _Health = TotalHealth;
        }
        healthUI.sizeDelta = new Vector2(heartSize * _Health, heartSize);
        Debug.Log("El jugador recupero vida. Su salud actual es de " + _Health);
    }

    private IEnumerator VisualFeedback()
    {
        _Renderer.color = Color.red;

        yield return new WaitForSeconds(0.1f);

        _Renderer.color = Color.white;
    }

    public void RespawnPoint()
    {
        _Rigidbody.position = SpawnPoint.position;
    }

    private void OnEnable()
    {
        _Health = TotalHealth;
        AddHealth(3);
        RespawnPoint();
    }

    private void OnDisable()
    {
        gameOverMenu.gameObject.SetActive(true);
        Enemies.SetActive(false);
        Items.SetActive(false);
        _Animator.enabled = false;
        _Controller.enabled = false;
        _Audio.enabled = false;
        _Renderer.color = Color.white;
    }
}
