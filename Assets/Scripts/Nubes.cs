﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nubes : MonoBehaviour
{
    public float minX;
    public float maxX;
    public float TiempoEspera;
    public float speed = 0.02f;
    public Vector2 direccion = Vector2.right;
    private float lenght, startpos;
    public GameObject Cam;
    private Rigidbody2D _Rigidbody;
    // Start is called before the first frame update

    private void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
    }
    void Start()
    {
        startpos = transform.position.x;
        lenght = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float temp = (Cam.transform.position.x * (1 - speed));
        float distance = (Cam.transform.position.x * speed);
        Vector2 movimiento = direccion.normalized * speed;
        _Rigidbody.velocity = movimiento;
        if (temp > startpos + lenght) startpos += lenght;
        else if (temp < startpos - lenght) startpos -= lenght;
    }
}
