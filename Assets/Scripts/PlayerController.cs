﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    public float longIdleTime = 5f;
    public float speed = 2f;
    public float jumpForce = 4f;

    //Suelo
    public Transform GroundCheck;
    public LayerMask GroundLayer;
    public float GroundCheckRadius;

    //Referencias
    private Rigidbody2D _Rigidbody;
    private Animator _Animator;

    //Long Idle
    private float _LongIdleTimer;

    //Movimiento
    private Vector2 _Movimiento;
    private bool _MirandoDerecha = true;
    private bool IsGrounded;
    private AudioSource _Audio;

    //Ataque
    private bool _IsAttacking;

    void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
        _Animator = GetComponent<Animator>();
        _Audio = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(_IsAttacking == false) {
            //Movimiento del personaje
            float horizontalInput = CrossPlatformInputManager.GetAxisRaw("Horizontal");
            _Movimiento = new Vector2(horizontalInput, 0f);

            //Girar al personaje
            if (horizontalInput < 0f && _MirandoDerecha == true)
            {
                Flip();
            }
            else if (horizontalInput > 0f && _MirandoDerecha == false)
            {
                Flip();
            }
        }
        //Esta en el suelo
        IsGrounded = Physics2D.OverlapCircle(GroundCheck.position, GroundCheckRadius, GroundLayer);

        //Salto
        if (CrossPlatformInputManager.GetButtonDown("Jump") && IsGrounded == true && _IsAttacking == false)
        {
            _Rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
        
        //Ataque
        if(CrossPlatformInputManager.GetButtonDown("Fire1") && IsGrounded == true && _IsAttacking == false)
        {
            _Movimiento = Vector2.zero;
            _Rigidbody.velocity = Vector2.zero;
            _Animator.SetTrigger("Attack");
        }
    }

    void FixedUpdate()
    {
        if (_IsAttacking == false)
        {
            float horizontalVelocity = _Movimiento.normalized.x * speed;
            _Rigidbody.velocity = new Vector2(horizontalVelocity, _Rigidbody.velocity.y);
        }
    }

    void LateUpdate()
    {
        _Animator.SetBool("Idle", _Movimiento == Vector2.zero);
        _Animator.SetBool("IsGrounded", IsGrounded);
        _Animator.SetFloat("VerticalVelocity", _Rigidbody.velocity.y);

        //Detecta el estado de animación, si esta atacando activa el bool para que no pueda atacar en ese momento
        if (_Animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            _Audio.Play();
            _IsAttacking = true;
        }
        else
        {
            _IsAttacking = false;
        }

        //Long Idle
        if (_Animator.GetCurrentAnimatorStateInfo(0).IsTag("Idle"))
        {
            _LongIdleTimer += Time.deltaTime;

            if (_LongIdleTimer >= longIdleTime)
            {
                _Animator.SetTrigger("LongIdle");
            }
        }
        else
        {
            _LongIdleTimer = 0f;
        }

    }

    private void Flip()
    {
        _MirandoDerecha = !_MirandoDerecha;
        float localScaleX = transform.localScale.x;
        localScaleX = localScaleX * -1f;
        transform.localScale = new Vector3(localScaleX, transform.localScale.y, transform.localScale.z);
    }
}
