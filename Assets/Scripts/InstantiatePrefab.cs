﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiatePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public Transform Point;
    public float LivingTime;


    public void Instantiate()
    {
        GameObject instantiatedObject = Instantiate(Prefab, Point.position, Quaternion.identity) as GameObject;
        if (LivingTime > 0f)
        {
            Destroy(instantiatedObject, LivingTime);
        }
    }
}
