﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayersCoins : MonoBehaviour
{
    private AudioSource _Audio;
    public AudioClip Money;
    public int Amount = 0;
    public TextMeshProUGUI CoinsUi;

    private void Awake()
    {
        _Audio = GetComponent<AudioSource>();
    }
    public void AddCoins(int Value)
    {
        Amount = Amount + Value;
        CoinsUi.text = ("x" + Amount);
        _Audio.clip = Money;
        _Audio.Play();
        Debug.Log("Tengo " + Amount + " monedas");
    }
}
